import { Component, OnInit } from '@angular/core';

import {  FormControl, FormGroup,Validators, FormBuilder }  from '@angular/forms';
import { NgForm } from '@angular/forms';
import { FormserviceService } from './formservice.service';
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
  
})
export class FormComponent implements OnInit {
    regform : FormGroup;
    items:any;
    tabledata: any[] = []; // Your data array
    currentPage = 1; // Current page
    itemsPerPage = 5; // Number of items to display per page
  constructor(private fb:FormBuilder,private is :FormserviceService) { }
  
  ngOnInit(): void {
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.data = [
      {
        "country": "01",
        "litres": 0
      },
      {
        "country": "05",
        "litres": "1k"
      },
      {
        "country": "10",
        "litres": "1k"
      },
      {
        "country": "15",
        "litres": "2k"
      },
      {
        "country": "20",
        "litres": "3k"
      },
      {
        "country": "25",
        "litres": "4k"
      },
      {
        "country": "30",
        "litres": "4k"
      }
    ];
    
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "country";
    categoryAxis.title.text = "Date";
    
    // Customizing category labels
    categoryAxis.renderer.labels.template.adapter.add("text", function(text) {
      if (text == "01") return "01";
      if (text == "05") return "05";
      if (text == "10") return "10";
      if (text == "15") return "15";
      if (text == "20") return "20";
      if (text == "25") return "25";
      if (text == "30") return "30";
      return text;
    });
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Finances";
    valueAxis.renderer.minGridDistance = 30;
    let series = chart.series.push(new am4charts.LineSeries());
    series.name = "Income";
    series.dataFields.valueY = "litres";
    series.dataFields.categoryX = "country";
    series.bullets.push(new am4charts.CircleBullet());
    series.tooltipText = "Series: {name}\nCategory: {categoryX}\nValue: {valueY}";
    chart.scrollbarY = new am4core.Scrollbar();
    chart.responsive.enabled = true;
    chart.width = am4core.percent(50);
    chart.height = am4core.percent(100);
let series2 = chart.series.push(new am4charts.LineSeries());
series2.name = "Outcome"; 
series2.dataFields.valueY = "expenses"; 
series2.dataFields.categoryX = "country";
series2.bullets.push(new am4charts.CircleBullet());
series2.tooltipText = "Series: {name}\nCategory: {categoryX}\nValue: {valueY}";
chart.legend = new am4charts.Legend();

    this.is.getapi1().subscribe((data)=>{
      console.log(data);
      this.items=data;
    })
    this.is.getapi2().subscribe((data)=>{
      console.log(data);
      this.tabledata=data;
    })
  }
  
get totalPages(): number {
  return Math.ceil(this.tabledata.length / this.itemsPerPage);
}
get displayedData(): any[] {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  return this.tabledata.slice(startIndex, endIndex);
}
previousPage() {
  if (this.currentPage > 1) {
    this.currentPage--;
  }
}
nextPage() {
  if (this.currentPage < this.totalPages) {
    this.currentPage++;
  }
}

}

import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { BehaviorSubject,Observable,tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormserviceService {

  constructor(private _httpClient: HttpClient) { }


  getapi1(): Observable<any[]> {              
    const apiUrl = `https://1.api.fy23ey01.careers.ifelsecloud.com/`;    
     return this._httpClient.get<any[]>(apiUrl);  
  }
 

  getapi2(): Observable<any[]> {              
    const apiUrl = ` https://2.api.fy23ey01.careers.ifelsecloud.com/`;    
     return this._httpClient.get<any[]>(apiUrl);  
  }
}
